<?php

return [

    'auto'     => [
        'title'          => 'Autos',
        'title_singular' => 'Auto',
        'fields'         => [
            'id'                => 'ID',
            'type'              => 'Type',
            'make'              => 'Make',
            'model'             => 'Model',
            'vin'               => 'VIN',
            'first_registration'=> 'First Registration',
            'purchase_date'     => 'Purchase Date',
            'documents'         => 'Documents',
            'created_at'        => 'Created at',
            'updated_at'        => 'Updated at',
        ],
    ],
    'driver'     => [
        'title'          => 'Drivers',
        'title_singular' => 'Driver',
        'fields'         => [
            'id'                        => 'ID',
            'first_name'                => 'First Name',
            'last_name'                 => 'Last Name',
            'social_security_number'    => 'Social Security Number',
            'created_at'                => 'Created at',
            'updated_at'                => 'Updated at',
        ],
    ],
];
