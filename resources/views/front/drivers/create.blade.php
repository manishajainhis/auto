@extends('layouts.app')

@section('content')
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <h2 class="modal-title">{{ trans('panel.add') }} {{ trans('cruds.driver.title_singular') }}</h2>
            </div>
            <form id="createDriverForm"  action="{{ route('drivers.store') }}" method="POST" role="form">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <input type="text" name="first_name" class="form-control" placeholder="First Name" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <input type="text" name="last_name" class="form-control" placeholder="Last Name" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-lock"></i></span>
                            </div>
                            <input type="text" name="social_security_number" class="form-control" placeholder="Social Security Number" />
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer text-center justify-content-center">
                    <button type="submit" class="btn btn-success btn-lg">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
@parent
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#createDriverForm').validate({
           onfocusout: false,
           errorPlacement: function(label, element) {
                label.addClass('help-block validation-error');
                if ($(element).is('select') && $(element).hasClass('select2')) {
                    label.insertAfter(element.siblings('.select2'));
                } else if ($(element).is('select')) {
                    label.insertAfter(element.parents('.input-group'));
                } else {
                    label.insertAfter(element.parents('.input-group'));
                }
           },
           rules: {
              "first_name": "required",
              "last_name": "required",
              "social_security_number": "required",
           },
           messages: {
              "first_name": "Please fill first name",
              "last_name": "Please fill last name",
              "social_security_number": "Please fill social security number",
           }
        });
    });
</script>
@endsection