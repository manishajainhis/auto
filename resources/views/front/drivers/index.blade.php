@extends('layouts.app')

@section('content')
    <div class="row">
      	<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
	      	<div class="card">
	          <div class="card-header card-header-primary">
	            <h4 class="card-title text-center">
	              {{ trans('cruds.driver.title_singular') }} {{ trans('panel.list') }}
	            </h4>

	            <div class="card-body">
		            <div class="row justify-content-end">
		              <a class="btn btn-primary m-2" href="{{ route('drivers.create') }}" data-toggle="tooltip" rel="tooltip" data-placement="top" title="{{ trans('global.add') }}"> Add Driver </a>
		            </div>
		            <br />
		            <div class="table-responsive">
		            	<table class="table table-striped table-bordered dt-responsive">
		            		<thead>
						      <tr>
						      	<th>#</th>
						        <th>{{ trans('cruds.driver.fields.first_name') }}</th>
						        <th>{{ trans('cruds.driver.fields.last_name') }}</th>
						        <th>{{ trans('cruds.driver.fields.social_security_number') }}</th>
						      </tr>
						    </thead>
						    <tbody>
						    	@if($drivers->count() > 0)
						    		@foreach($drivers as $key => $driver)
								    	<tr>
								    		<td>{{ $key+1 }}</td>
								        	<td>{{ $driver->first_name ?? "" }}</td>
								        	<td>{{ $driver->last_name ?? "" }}</td>
								        	<td>{{ $driver->social_security_number ?? "" }}</td>
								      	</tr>
						      		@endforeach
						      	@else
						      		<tr class="text-center">
						      			<td colspan="4">No driver available</td>
						      		</tr>
						      	@endif
						    </tbody>
		            	</table>
		            </div>
	          </div>
	      	</div>
      	</div>
  </div>
@endsection