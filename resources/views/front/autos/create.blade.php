@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.css" />
@endsection

@section('content')
    <div class="modal-dialog-lg">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <h2 class="modal-title">{{ trans('panel.add') }} {{ trans('cruds.auto.title_singular') }}</h2>
            </div>
            <form id="createAutoForm" action="{{ route('autos.store') }}" role="form" files=true enctype="multipart/form-data" method="post">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group input-group-cust">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-car"></i></span>
                                    </div>
                                    {!! Form::select('type',$autoTypes,null, ['placeholder' => 'Select Auto Type','class'=>'form-control','required'=>'required']) !!}
                                </div>
                                @if($errors->has('type'))
                                <div class="help-block text-danger">
                                    {{ $errors->first('type') }}
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group input-group-cust">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-car"></i></span>
                                    </div>
                                    {!! Form::text('make',null,['placeholder' => 'Make','class'=>'form-control','required'=>'required']) !!}
                                </div>
                                @if($errors->has('make'))
                                <div class="help-block text-danger">
                                    {{ $errors->first('make') }}
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group input-group-cust">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-car"></i></span>
                                    </div>
                                    {!! Form::selectYear('model', 1990, date('Y'),null,['placeholder' => 'Select Model','class'=>'form-control','required'=>'required']) !!}
                                </div>
                                @if($errors->has('model'))
                                <div class="help-block text-danger">
                                    {{ $errors->first('model') }}
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group input-group-cust">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-car"></i></span>
                                    </div>
                                    {!! Form::text('vin',null,['placeholder' => 'VIN','class'=>'form-control','required'=>'required']) !!}
                                </div>
                                @if($errors->has('vin'))
                                <div class="help-block text-danger">
                                    {{ $errors->first('vin') }}
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group input-group-cust">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-car"></i></span>
                                    </div>
                                    {!! Form::text('first_registration',null,['placeholder' => 'First Registration','class'=>'form-control','required'=>'required']) !!}
                                </div>
                                @if($errors->has('first_registration'))
                                <div class="help-block text-danger">
                                    {{ $errors->first('first_registration') }}
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group input-group-cust">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar-day"></i></span>
                                    </div>
                                    {!! Form::text('purchase_date',null,['placeholder' => 'Purchase Date','class'=>'form-control purchase_date','required'=>'required','autoComplete'=>'off']) !!}
                                </div>
                                @if($errors->has('purchase_date'))
                                <div class="help-block text-danger">
                                    {{ $errors->first('purchase_date') }}
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="documents">
                        <div class="document-heading">
                            <h4>Upload Documents</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @foreach($documentTypes as $key => $documentType)
                                    <div class="row">
                                        <div class="col-md-6">  
                                            <div class="row">
                                                <label class="control-label col-sm-5">{{ $documentType }}</label>
                                                <div class="col-sm-7"> 
                                                    <div class="form-group input-group-cust"> 
                                                        <input type="file" class="form-control-file" accept="image/png,image/jpg,image/jpeg,application/pdf,application/docx" name="documents[{{$key}}]" required />
                                                    </div>
                                                    @if($errors->has('documents.'.$key))
                                                    <div class="help-block text-danger">
                                                        {{ $errors->first('documents.'.$key) }}
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <label class="control-label col-sm-3">Expiry Date</label>
                                                <div class="col-sm-9"> 
                                                    <div class="form-group input-group-cust"> 
                                                        {!! Form::text('expiries['.$key.']',null,['placeholder' => 'Expiry Date','class'=>'form-control datepicker','required'=>'required','autoComplete'=>'off']) !!}
                                                    </div>
                                                    @if($errors->has('expiries.'.$key))
                                                    <div class="help-block text-danger">
                                                        {{ $errors->first('expiries.'.$key) }}
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-center justify-content-center">
                    <button type="submit" class="btn btn-success btn-lg">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.purchase_date').datepicker({
            "format" : "dd-MM-YYYY",
            "endDate" : new Date(),
            'autoHide': true,
        });

        $('.datepicker').datepicker({
            "format" : "dd-MM-YYYY",
            "startDate" : new Date(),
            'autoHide': true,
        });
        
        /* Form Validate */
		$.validator.addMethod('filesize', function (value, element, param) {
			console.log(element.files[0].size);
			return this.optional(element) || (element.files[0].size <= param)
		}, 'File size must be less than {0}');

        $('#createAutoForm').validate({
           onfocusout: false,
           errorPlacement: function(label, element) {
                console.log($(element).is('file'));
                label.addClass('help-block validation-error');
                if ($(element).is('select') && $(element).hasClass('select2')) {
                    label.insertAfter(element.siblings('.select2'));
                } else if ($(element).is('select')) {
                    label.insertAfter(element.parents('.input-group-cust'));
                }else if ($(element).is('file')) {
                    label.insertAfter(element);
                } else {
                    label.insertAfter(element.parents('.input-group-cust'));
                }
           },
           rules: {
              "type": "required",
              "make": "required",
              "model": "required",
              "vin": "required",
              "first_registration": "required",
              "purchase_date": "required",
			  "documents[]": {
                "required": true,
                "extension": "png,jpg,jpeg,pdf,docx",
                "maxsize": 50,
              },
              "expiries[]": "required",
           },
           messages: {
              "type": "Please select auto type",
              "make": "Please fill make",
              "model": "Please fill model",
              "vin": "Please enter vin number",
              "first_registration": "Please fill first registration",
              "purchase_date": "Please fill purchase date",
			  "documents[]": {
                   "required": "Please upload document",
                   "extension": "Please upload valid file",
                   "maxsize": "Size wrong",
               },
              "expiries[]": "Please fill expiry date of document",
           }
        });
    });
</script>
@endsection