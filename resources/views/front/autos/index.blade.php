@extends('layouts.app')

@section('content')
    <div class="row">
      	<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
	      	<div class="card">
	          <div class="card-header card-header-primary">
	            <h4 class="card-title text-center">
	              {{ trans('cruds.auto.title_singular') }} {{ trans('panel.list') }}
	            </h4>

	            <div class="card-body">
		            <div class="row justify-content-end">
		              <a class="btn btn-primary m-2" href="{{ route('autos.create') }}" data-toggle="tooltip" rel="tooltip" data-placement="top" title="{{ trans('global.add') }}"> Add Auto </a>
		            </div>
		            <br />
		            <div class="table-responsive">
		            	<table class="table table-striped table-bordered dt-responsive">
		            		<thead>
						      <tr>
						        <th>#</th>
						        <th>{{ trans('cruds.auto.fields.type') }}</th>
						        <th>{{ trans('cruds.auto.fields.make') }}</th>
						        <th>{{ trans('cruds.auto.fields.model') }}</th>
						        <th>{{ trans('cruds.auto.fields.vin') }}</th>
						        <th>{{ trans('cruds.auto.fields.purchase_date') }}</th>
						        <th>{{ trans('cruds.auto.fields.created_at') }}</th>
						      </tr>
						    </thead>
						    <tbody>
						    	@if($autos->count() > 0)
						    		@foreach($autos as $key => $auto)
								    	<tr>
								        	<td>{{ $key+1 }}</td>
								        	<td>{{ config('constants.auto_types')[$auto->type] ?? "" }}</td>
								        	<td>{{ $auto->make ?? "" }}</td>
								        	<td>{{ $auto->model ?? "" }}</td>
								        	<td>{{ $auto->vin ?? "" }}</td>
								        	<td>{{ date('d-M-Y',strtotime($auto->purchase_date)) ?? "" }}</td>
								        	<td>{{ date('d-M-Y',strtotime($auto->created_at)) ?? "" }}</td>
								      	</tr>
						      		@endforeach
						      	@else
						      		<tr class="text-center">
						      			<td colspan="7">No auto available</td>
						      		</tr>
						      	@endif
						    </tbody>
		            	</table>
		            </div>
	          </div>
	      	</div>
      	</div>
  </div>
@endsection