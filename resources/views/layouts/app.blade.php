 <!DOCTYPE html>
<html lang="en">
    <head>
        <title>Auto Parts</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
        <link rel="stylesheet" href="{{ asset('assets/front/style.css') }}">
        @yield('styles')
    </head>
    <body>

        <div class="page-wrapper compact-wrapper" id="pageWrapper">
            @include('partials.header')
          <!-- Main content -->
            <section class="content py-4" style="padding-top: 30px">
                <div class="page-body">
                    <div class="container">
                        @if(session('success'))                            
                            <div class="row mb-2">
                                <div class="col-lg-12">
                                    <div class="alert alert-success" role="alert">{{ session('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span class="text-white" aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>                
                        @endif

                        @yield("content")
                    </div>
                </div>
            </section>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js"></script>
        @yield('scripts')
    </body>
</html>