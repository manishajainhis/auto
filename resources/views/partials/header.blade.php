<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item {{ request()->is('drivers*') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('drivers.index') }}">{{ trans('cruds.driver.title') }}</a>
    </li>
    <li class="nav-item {{ request()->is('autos*') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('autos.index') }}">{{ trans('cruds.auto.title') }}</a>
    </li>
  </ul>
</nav>