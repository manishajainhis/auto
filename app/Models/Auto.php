<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \DateTimeInterface;

class Auto extends Model
{
    use HasFactory;

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'type',
        'cap_tractor',
        'special_truck',
        'little_truck',
        'car',
        'semitraier',
        'trailer',
        'make',
        'model',
        'vin',
        'first_registration',
        'purchase_date',
    ];

    public function documents()
    {
        return $this->morphMany(Uploads::class, 'uploadsable');
    }

}
