<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Front\Auto\StoreRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Auto;
use App\Models\Uploads;
use Validator;

class AutosController extends Controller
{
    public function __construct()
    {
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $autos = Auto::all();
        return view('front.autos.index',compact('autos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $autoTypes = config('constants.auto_types');
        $documentTypes = config('constants.auto_document_types');
        return view('front.autos.create',compact(['autoTypes','documentTypes']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $inputs = $request->all();
        $inputs['purchase_date'] = date('Y-m-d',strtotime($inputs['purchase_date']));
        $auto = Auto::create($inputs);
        if($auto){
            if($request->hasFile('documents') && count($request->documents) > 0){
                foreach ($request->file('documents') as $file_type => $file) {
                    $this->uploadDocument($auto, $file,'auto-documents',$file_type);
                }
            }
        }
        return redirect()->route('autos.index')->with('success',trans('cruds.auto.title_singular')." ".trans('panel.add_succes'));
    }

    public function uploadDocument($directory, $file, $folder,$file_type){
        $upload               = new Uploads; 
        $upload->path         = $file->store($folder, 'public');
        $upload->extension    = $file->getClientOriginalExtension();        
        $upload->upload_type  = $file_type;        
        $upload->expires_at   = date('Y-m-d',strtotime(request()->expiries[$file_type]));        

        $response             = $directory->documents()->save($upload);  
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Auto $auto)
    {
        // return view('admin.articles.show', compact('article'));
    }

    
}
