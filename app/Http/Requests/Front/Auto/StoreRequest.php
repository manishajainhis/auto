<?php

namespace App\Http\Requests\Front\Auto;

use App\Models\Auto;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreRequest extends FormRequest
{
    public function rules()
    {
		return [
            'type' =>[
                'required',
            ],
            'make'     => [
                'required',
            ],
            'model'     => [
                'required',
            ],
            'vin'     => [
                'required',
            ],
            'first_registration'     => [
                'required',
            ],
            'purchase_date'     => [
                'required',
            ],
            'documents.*' => [
                'required',
                'file',
                'mimes:jpg,png,pdf,docx',
                "max:10000"
            ],
            'expiries.*' => [
                'required',
            ],
        ];

    }

    public function attributes(){
        return [
            'type' => 'Auto Type',
            'expiries.*' => 'Document Expiry Date',
            'documents.*' => 'Document',
        ];
    }

}

