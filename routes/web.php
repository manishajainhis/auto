<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\Front\DriversController;
use App\Http\Controllers\Front\AutosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {          
    return redirect()->route('drivers.index'); 
})->name('home');

Route::resource('drivers', DriversController::class)->only(['index','create','store']);

Route::resource('autos', AutosController::class)->only(['index','create','store']);