<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Constants 
    |--------------------------------------------------------------------------
    |
    |
    */
    'auto_types' => [
        1             => "Cap Tractor",
        2             => "Autoutilitară > 3,5 to",
        3             => "Autoutilitară",
        4             => "Autoturism",
        5             => "Semiremorcă",
        6             => "Remorcă",
    ],

    'auto_document_types' => [
        "rca"                   => "RCA",
        "itp"                   => "ITP",
        "carry_good_license"    => "Copie conformă",
        "tacho_valability"      => "Verificare tahograf",
        "road_tax"              => "RoVinietă",
        "port_tax"              => "Intrare port",
    ],

];
