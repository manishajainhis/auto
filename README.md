# Laravel HIPL Dashboard
Develop By __HIPL__


## How to use

- Clone the repository with _git clone_
- Copy _.env.example_ file to _.env_ and edit database credentials there
- Run _composer install_
- Run _composer update_
- Run _php artisan key:generate_
- Run _php artisan migrate --seed_ (it has some seeded data for your testing)
- That's it: launch the main URL. 
- Deploye on live server then debug mode off. Please open .env file and replace the code APP_DEBUG=false

## License

Basically, feel free to use and re-use any way you want.

---

## More from our HIPL Team

- Check out our website [Helpful Insight Solution](helpfulinsightsolution.com)
- Read our [Blog](helpfulinsightsolution.com/blogs)